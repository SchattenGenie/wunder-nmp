from typing import Union
import torch.nn as nn
import torch
import numpy as np
from .message_passer import *
from .state_updater import *
from .constants import *

class Node(object):
    def __init__(self, name: str, device, data: Union[np.ndarray, torch.Tensor] = None, shape: int = None, type: str = None):
        """
        Initialization of Node object. If tensor if provided init with it. If shape is provided placeholder is created.
        If both provided tf.Tensor goes firts.
        :param name: str, unique name of node
        :param tensor: tf.Tensor object or None, initialization for self.state
        :param message_shape: int. Size of message(output of message function).
                                 Forces message passing to be of this size.
        :param shape:int or None. Provided for tf.Placeholder
        :param dtype: str, default 'base'. Type of Node in case graph has nodes of different type.
        """
        self.name = name
        self.type = type
        self.messages_in = []
        self.messages_out = []
        self.state_updater = None
        self.edges_in = []
        self.edges_out = []
        self.device = device
        
        if isinstance(data, torch.Tensor):
            self.data = data.detach().numpy()
            self.state = torch.tensor(self.data, dtype=torch.float32).to(self.device)
            self.shape = len(self.state)
        elif isinstance(data, np.ndarray):
            self.data = data
            self.state = torch.tensor(self.data, dtype=torch.float32).to(self.device)
            self.shape = len(self.state)
        elif isinstance(shape, int):
            self.state = torch.zeros(shape).to(self.device)
            self.shape = shape
        else:
            raise ValueError('data or shape should be provided.')

    def reset(self):
        self.state = torch.tensor(self.data, dtype=torch.float32).to(self.device)

    def register_message(self, message: torch.Tensor, message_type: str):
        if message_type == IN_MESSAGE:
            self.messages_in.append(message)
        elif message_type == OUT_MESSAGE:
            self.messages_out.append(message)
        else:
            raise ValueError('Unknown message type {}'.format(message_type))

    def set_state_updater(self, state_updater):
        self.state_updater = state_updater

    def update(self, messages_in: Union[list, torch.Tensor] = None, messages_out: Union[list, torch.Tensor] = None):
        """
        :return:
        """

        if self.state_updater is None:
            raise ValueError("State updater for node {} is not provided".format(self.name))

        # aggregation
        
        self.state = self.state_updater.update_state(
            self.messages_in if messages_in is None else messages_in, 
            self.messages_out if messages_out is None else messages_out, 
            node=self
        )

        # zero messages
        self.messages_in.clear()
        self.messages_out.clear()

        return self.state

