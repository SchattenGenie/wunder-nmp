def loss_similar_kl(nodes_predictions, true_labels, clusters):
    true_labels = np.array(true_labels)
    nodes_means, nodes_log_std = list(zip(*nodes_predictions))
    nodes_means = np.array(nodes_means)
    nodes_log_std = np.array(nodes_log_std)
    loss = 0.
    for label in clusters.keys():
        labeled_means = torch.stack(list(nodes_means[true_labels==label]))
        labeled_log_std = torch.stack(list(nodes_log_std[true_labels==label]))

        l = clusters[label]['std'].log() - labeled_log_std

        l = l + (labeled_log_std.exp().pow(2) + 
                 (labeled_means - clusters[label]['mean']).pow(2)) / (2. * clusters[label]['std'].pow(2))
        loss = loss + l.sum(dim=1).mean()
    del nodes_log_std, nodes_means
    return loss

def loss_similar_kl_inv(nodes_predictions, true_labels, clusters):
    true_labels = np.array(true_labels)
    nodes_means, nodes_log_std = list(zip(*nodes_predictions))
    nodes_means = np.array(nodes_means)
    nodes_log_std = np.array(nodes_log_std)
    loss = 0.
    for label in clusters.keys():
        labeled_means = torch.stack(list(nodes_means[true_labels==label]))
        labeled_log_std = torch.stack(list(nodes_log_std[true_labels==label]))

        l = labeled_log_std - clusters[label]['std'].log()

        l = l + (clusters[label]['std'].pow(2) + (labeled_means - 
                                              clusters[label]['mean']).pow(2)) / (2. * labeled_log_std.exp().pow(2))
        loss = loss + l.sum(dim=1).mean()
    del nodes_log_std, nodes_means
    return loss

def loss_similar_kl_sim(nodes_predictions, true_labels, clusters):
    true_labels = np.array(true_labels)
    nodes_means, nodes_log_std = list(zip(*nodes_predictions))
    nodes_means = np.array(nodes_means)
    nodes_log_std = np.array(nodes_log_std)
    loss = 0.
    for label in clusters.keys():
        labeled_means = torch.stack(list(nodes_means[true_labels==label]))
        labeled_log_std = torch.stack(list(nodes_log_std[true_labels==label]))
        # log (\sigma_1 / \sigma_2)
        l = (clusters[label]['std'].pow(2) + (labeled_means - 
                                          clusters[label]['mean']).pow(2)) / (2. * labeled_log_std.exp().pow(2))
        # 
        l = l + (labeled_log_std.exp().pow(2) + (labeled_means - 
                                                clusters[label]['mean']).pow(2)) / (2. * clusters[label]['std'].pow(2))
        loss = loss + l.sum(dim=1).mean()
    del nodes_log_std, nodes_means
    return loss

def loss_distinct_kl_sim(clusters):
    loss = 0.
    for label_1 in clusters.keys():
        for label_2 in clusters.keys():
            if label_1 == label_2:
                continue
            l = (clusters[label_2]['std'].pow(2) - 
                 (clusters[label_2]['mean'] - clusters[label_1]['mean']).pow(2)) / (2. * clusters[label_1]['std'].pow(2))
            l = l + (clusters[label_1]['std'].pow(2) - 
                     (clusters[label_2]['mean'] - clusters[label_1]['mean']).pow(2)) / (2. * clusters[label_2]['std'].pow(2))
            loss = loss + torch.relu(margin - l.sum())
            
    del nodes_log_std, nodes_means
    return loss