import numpy as np
from typing import Union
from collections import defaultdict
from .edge import *
from .node import *
from typing import Callable
import torch.nn as nn
import torch

class StateUpdater(object):
    def __init__(self, input: int, output: int, state_updater: nn.Module, device):
        """
        :param scope:
        """
        self.input = input
        self.output = output
        self.device = device
        self.state_updater = state_updater(input * 2, output).to(self.device)
        self.zero = torch.tensor(np.zeros((1, self.input)), dtype=torch.float32).to(self.device)
        
    def aggregate_messages(self, messages: Union[list, torch.Tensor]):
        """
        message aggregator
        :return:
        """
        # return max of concatenaded messages
        if isinstance(messages, list) and messages:
            return torch.stack(messages).max(dim=0)[0][None, :]
        elif isinstance(messages, torch.Tensor) and len(messages):
            return messages.max(dim=0)[0][None, :]
        else:
            return self.zero

    def update_state(self, messages_in: Union[list, torch.Tensor], messages_out:Union[list, torch.Tensor], node: 'Node'):
        aggregated_messages_in = self.aggregate_messages(messages=messages_in)
        aggregated_messages_out = self.aggregate_messages(messages=messages_out)
        
        return self.state_updater(torch.cat([aggregated_messages_in, aggregated_messages_out], 1), node.state[None, :])[0]