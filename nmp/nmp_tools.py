import networkx as nx
import numpy as np
from .edge import *
from .node import *
from .message_passer import *
from .state_updater import *
from collections import defaultdict

def get_lablesx(graphx):
    """
    return dict of {node id: 'signal'}
    """
    lablesx = dict()
    for node_id, node in graphx.nodes(data=True):
        lablesx[node_id] = node['signal']
    return lablesx


def get_central_node(lablesx):
    """
    return node id uniformly over labels
    """
    lablesx_np = np.array(list(lablesx.values()))
    possible_labels=np.unique(lablesx_np)
    return list(lablesx.keys())[np.random.choice(np.where(lablesx_np == np.random.choice(possible_labels))[0])]


def unique_neighbours_of_k_order(graphx: nx.DiGraph, node: int, order: int):
    """
    return list of node id's which contain all neighbours of k order.
    """
    neighbours = set([node])
    neighbours_layers_sets = list([neighbours])
    if nx.is_directed(graphx):
        get_neighbours = lambda graphx, node: list(graphx.successors(node)) + list(graphx.predecessors(node))
    else:
        get_neighbours = lambda graphx, node: list(graphx.neighbors(node))
    for k in range(order):
        neighbours = set((neighbour 
                          for node in neighbours 
                          for neighbour in get_neighbours(graphx, node) 
                         ))
        neighbours_layers_sets.append(neighbours)
    
    neighbours = defaultdict(lambda: -1)
    cumulative = set()
    k = 0
    for layer in neighbours_layers_sets:
        for node in (layer - cumulative):
            neighbours[node] = k
        cumulative = cumulative.union(layer)
        k += 1
        
    return neighbours

def get_random_subgraphx(graphx: nx.Graph, lablesx: dict, 
                        order: int = 1, balanced=True, **kwargs):
    if balanced:
        central_node = get_central_node(lablesx=lablesx)
    else:
        central_node = np.random.choice(list(lablesx.keys()))
        
    assert order > 0
    
    k_order_neighbours = unique_neighbours_of_k_order(graphx, central_node, order)    
    subgraphx = graphx.subgraph(list(k_order_neighbours.keys()))
    
    # add torch nodes to subgraph attributes
    for node_id, node in subgraphx.nodes(data=True):
        node['order'] = k_order_neighbours[node_id]
        
    return subgraphx    


from math import fabs, sqrt, log
def rms_integral_root_closed_py(basetrack_left, basetrack_right, 
                             TX_LEFT='TX', TY_LEFT='TY',
                             TX_RIGHT='TX', TY_RIGHT='TY'):
    DISTANCE = 1293.
    EPS = 1e-6
    dz = basetrack_right['features']['SZ'] - basetrack_left['features']['SZ']
    dx = basetrack_left['features']['SX'] - (basetrack_right['features']['SX'] - basetrack_right['features'][TX_RIGHT] * dz)
    dy = basetrack_left['features']['SY'] - (basetrack_right['features']['SY'] - basetrack_right['features'][TY_RIGHT] * dz)
    dtx = (basetrack_left['features'][TX_LEFT] - basetrack_right['features'][TX_RIGHT])
    dty = (basetrack_left['features'][TX_LEFT] - basetrack_right['features'][TY_RIGHT])
    # dz can be assigned to arbitrary value, acutally !
    dz = DISTANCE
    a = (dtx * dz) ** 2 + (dty * dz) ** 2
    b = 2 * (dtx * dz * dx +  dty * dz * dy)
    c = dx ** 2 + dy ** 2
    if a == 0.:
        return fabs(sqrt(c))
    discriminant = (b ** 2 - 4 * a * c)
    log_denominator = 2 * sqrt(a) * sqrt(a + b + c) + 2 * a + b + EPS
    log_numerator = 2 * sqrt(a) * sqrt(c) + b + EPS
    first_part = ( (2 * a + b) * sqrt(a + b + c) - b * sqrt(c) ) / (4 * a)
    if fabs(discriminant) < EPS:
        return fabs(first_part)
    try: 
        result = fabs((discriminant * log(log_numerator / log_denominator) / (8 * sqrt(a * a * a)) + first_part))
        return result
    except:
        print(basetrack_left)
        print(basetrack_left)
        return 1e6
    
from tqdm import tqdm
def preprocess_graphx(graphx: nx.DiGraph, 
                      node_shape: int, 
                      edge_shape: int, 
                      layers: float,
                      threshold: float,
                      message_passers: dict, 
                      state_updater: StateUpdater,
                      node_feature_exctractor: Callable, 
                      edge_feature_exctractor: Callable,
                      device,
                      order: int = -1,
                      balanced=True, **kwargs):
    """
    if order <= 0 => the whole graph is proceeded
    """
    DISTANCE = 1293.
    edges_to_add = []
    for node_id_left, node_left in tqdm(graphx.nodes(data=True), leave=False):
        for node_id_right, node_right in graphx.nodes(data=True):
            if node_left['features']['SZ'] > node_right['features']['SZ']:
                node_left, node_right = node_right, node_left
                node_id_right, node_id_left = node_id_left, node_id_right
            r = rms_integral_root_closed_py(node_left, node_right)
            if fabs(node_right['features']['SZ'] - node_left['features']['SZ']) > DISTANCE * layers:
                continue
            edge = {
                'weight': rms_integral_root_closed_py(node_left, node_right),
                'dsx': (node_right['SX'] - node_left['SX']) / DISTANCE,
                'dsy': (node_right['SY'] - node_left['SY']) / DISTANCE,
                'dsz': (node_right['SZ'] - node_left['SZ']) / DISTANCE,
                'dsxProjLeft': (node_right['SX'] - node_left['SX'] - (node_right['SZ'] - node_left['SZ']) * node_left['TX']) / (node_right['SZ'] - node_left['SZ']),
                'dsyProjLeft':(node_right['SY'] - node_left['SY'] - (node_right['SZ'] - node_left['SZ']) * node_left['TY']) / (node_right['SZ'] - node_left['SZ']),
                'dsxProjRight': (node_right['SX'] - node_left['SX'] - (node_right['SZ'] - node_left['SZ']) * node_right['TX']) / (node_right['SZ'] - node_left['SZ']),
                'dsyProjRight': (node_right['SY'] - node_left['SY'] - (node_right['SZ'] - node_left['SZ']) * node_right['TY']) / (node_right['SZ'] - node_left['SZ'])
            }
            edges_to_add.append((node_id_left, node_id_right, edge))
    graphx.add_edges_from(edges_to_add)
            
    X_nodes = []
    X_edges = []
    X_nodes_out = []
    X_nodes_in = []
    
    nodes_name_ids_conversion = {}
    nodes_ids_name_conversion = {}
    
    # add torch nodes to graphx attributes
    for i, (node_id, node) in enumerate(graphx.nodes(data=True)):
        nodes_name_ids_conversion[node_id] = i
        nodes_ids_name_conversion[i] = node_id
        node_torch = Node(
            name=str(node_id),
            data=node_feature_exctractor(node, node_shape=node_shape),
            device=device
        )
        node_torch.set_state_updater(state_updater=state_updater)
        node['node_torch'] = node_torch
        node['order'] = -1
        X_nodes.append(node_feature_exctractor(node, node_shape=node_shape))
    
    
    edges_name_ids_conversion = {}
    edges_ids_name_conversion = {}
    # add troch edges to subgraph attributes
    for i, (node_u_idx, node_v_idx, edge_uv) in enumerate(graphx.edges(data=True)):
        edges_name_ids_conversion[(node_u_idx, node_v_idx)] = i
        edges_ids_name_conversion[i] = (node_u_idx, node_v_idx)
        X_nodes_out.append(
            nodes_name_ids_conversion[node_u_idx]
        )
        X_nodes_in.append(
            nodes_name_ids_conversion[node_v_idx]
        )
        node_u_torch = graphx.node[node_u_idx]['node_torch']
        node_v_torch = graphx.node[node_v_idx]['node_torch']
        node_u_torch.edges_out.append(i)
        node_v_torch.edges_in.append(i)
        # create edge that connects node_u node_v
        edge_uv_torch = Edge(
            name=node_u_torch.name + '_' + node_v_torch.name, 
            data=edge_feature_exctractor(edge_uv, edge_shape),
            device=device
        )
        
        edge_uv_torch.set_message_passers(message_passers=message_passers, 
                                          node_to=node_u_torch, 
                                          node_from=node_v_torch)
        
        edge_uv['edge_torch'] = edge_uv_torch
        X_edges.append(edge_feature_exctractor(edge_uv, edge_shape))
    
    graphx.graph['X_nodes'] = torch.tensor(X_nodes, requires_grad=False, dtype=torch.float32).to(device)
    graphx.graph['X_edges'] = torch.tensor(X_edges, requires_grad=False, dtype=torch.float32).to(device)
    graphx.graph['X_nodes_out'] = X_nodes_out
    graphx.graph['X_nodes_in'] = X_nodes_in
    graphx.graph['message_passers'] = message_passers
    graphx.graph['state_updater'] = state_updater
    
    graphx.graph['nodes_name_ids_conversion'] = nodes_name_ids_conversion
    graphx.graph['nodes_ids_name_conversion'] = nodes_ids_name_conversion
    true_labels = []
    for _, node in graphx.nodes(data=True):
        true_labels.append(node['signal'])
    graphx.graph['true_labels'] = torch.LongTensor(true_labels)
    return graphx


def run_message_passing(subgraphx: nx.Graph, step: int):
    # passing messages
    for _, _, edge in subgraphx.edges(data=True):
        edge['edge_torch'].pass_message(step=step)
              
    # updating nodes
    for _, node in subgraphx.nodes(data=True):
        node['node_torch'].update()
    
    return True


def make_predictions(subgraphx: nx.Graph, readout: 'Readout', order: int, steps: int, **kwargs):
    """
    readout: Readout
    """
    nodes_predictions = []
    true_labels = []
    
    # make nodes predictions
    for node_id, node in subgraphx.nodes(data=True):
        
        if node['order'] <= order - steps or order < 0:
            nodes_predictions.append(readout.predict(node['node_torch']))
            true_labels.append(node['signal'])
    
    nodes_predictions = list(map(torch.stack, list(map(list, zip(*nodes_predictions)))))
    
    return nodes_predictions, np.array(true_labels)