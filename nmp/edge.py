from typing import Union
import torch.nn as nn
import torch
import numpy as np


class Edge(object):
    def __init__(self, name: str, device, data: Union[np.ndarray, torch.Tensor] = None, shape: int = None, type: str = None):
        """

        :param name:
        :param tensor:
        :param shape:
        :param dtype:
        """
        self.name = name
        self.type = type
        self.message_passer = None
        self.node_to = None
        self.node_from = None
        
        if isinstance(data, torch.Tensor):
            self.data = data.detach().numpy()
            self.state = torch.tensor(self.data, dtype=torch.float32).to(device) # .cuda()
            self.shape = len(self.state)
        elif isinstance(data, np.ndarray):
            self.data = data
            self.state = torch.tensor(self.data, dtype=torch.float32).to(device)
            self.shape = len(self.state)
        elif isinstance(shape, int):
            self.state = torch.zeros(shape).to(device)
            self.shape = shape
        else:
            raise ValueError('data or shape should be provided.')


    def register_message(self, message: torch.Tensor):
        pass

    def set_message_passers(self, message_passers: dict, node_to: 'Node', node_from: 'Node'):
        self.message_passers = message_passers
        self.node_to = node_to
        self.node_from = node_from
        
    def pass_message(self, step: int):
        if self.message_passers is None:
            raise ValueError("Message passer for edge {} is not provided".format(self.name))
        self.message_passers[step].pass_message(self.node_to, self.node_from, self)