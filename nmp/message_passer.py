import numpy as np
from collections import defaultdict
from .edge import *
from .node import *
from typing import Callable
from .constants import *
import torch.nn as nn
import torch

class MessagePasser(object):
    def __init__(self, node_to_size: int, node_from_size: int, edge_size: int, message_size: int, message_passer: nn.Module, device):
        """

        :param node_to:
        :param node_from:
        :param edge:
        :param message_passer:
        """
        self.node_to_size = node_to_size
        self.node_from_size = node_from_size
        self.edge_size = edge_size
        self.message_size = message_size
        self.message_passer = message_passer(self.node_to_size + self.node_from_size + self.edge_size, self.message_size).to(device)

    def pass_message(self, node_to: 'Node', node_from: 'Node', edge: 'Edge'):
        assert node_to.state.shape[0] == self.node_to_size
        assert node_from.state.shape[0] == self.node_from_size
        assert edge.state.shape[0] == self.edge_size
        # pass concatenated states to nn.Module
        # magic with [None, :] and following [0] is used cause some
        # torch.nn.* modules take only tensors with shape [batch_size, dim]
        message = self.message_passer(torch.cat([node_to.state, node_from.state, edge.state])[None, :])[0]
        node_to.register_message(message=message, message_type=IN_MESSAGE)
        node_from.register_message(message=message, message_type=OUT_MESSAGE)