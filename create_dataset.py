# coding: utf-8

#%matplotlib qt
# import seaborn
# from mpl_toolkits.mplot3d import Axes3D
# from mpl_toolkits.mplot3d.art3d import Line3DCollection
# from matplotlib import pylab as plt
import root_numpy
import pandas as pd
import numpy as np
import numpy.linalg as la
import copy
import pickle

BT_Z_unique = np.array([     0.,   1293.,   2586.,   3879.,   5172.,   6465.,   7758.,
                          9051.,  10344.,  11637.,  12930.,  14223.,  15516.,  16809.,
                         18102.,  19395.,  20688.,  21981.,  23274.,  24567.,  25860.,
                         27153.,  28446.,  29739.,  31032.,  32325.,  33618.,  34911.,
                         36204.,  37497.,  38790.,  40083.,  41376.,  42669.,  43962.,
                         45255.,  46548.,  47841.,  49134.,  50427.,  51720.,  53013.,
                         54306.,  55599.,  56892.,  58185.,  59478.,  60771.,  62064.,
                         63357.,  64650.,  65943.,  67236.,  68529.,  69822.,  71115.,
                         72408.,  73701.])
BRICK_X_MIN = 27762
BRICK_X_MAX = 72240
BRICK_Y_MIN = 3312
BRICK_Y_MAX = 76710
SAFE_M = 5000
dZ = 205

kwargs = {'bins' : 100, 'alpha' : 0.8, 'normed' : True}
BRICK_X = 124000
# Changed due to ignorance of 57th layer. Originally : #BRICK_Y = 99000
BRICK_Y = 76710
BRICK_Z = 75000
SAFE_M = 10000


# In[2]:


from tools.opera_tools import *


# In[3]:


pbg = load_bg(filepath='./data/', step=100)
pmc = load_mc(filename='./data/mcdata_taue2.root', step=1)


# In[4]:


idx_len_greater_100 = list()
for i in range(len(pmc)):
    if len(pmc.iloc[i]['BT_X']) > 100:
        idx_len_greater_100.append(pmc.iloc[i].Event_id)


# In[5]:


from create_graph.create_graph import create_graph

import networkx as nx
def gen_graph_nx(train_np, edges):
    G = nx.Graph()
    
    for i in range(len(train_np)): 
        G.add_node(i, features={
            'TX': train_np[i][4],
            'TY': train_np[i][5],
            'chi2': train_np[i][6]
        }, signal=train_np[i][7])
        
    for i in range(len(edges)):
        u = int(edges[i][0])
        v = int(edges[i][1])
        u_data = train_np[u]
        v_data = train_np[v]
        G.add_edge(u=edges[i][0], 
                   v=edges[i][1], 
                   features={
                       'dsx': abs(v_data[1] - u_data[1]) / 1000,
                       'dsy': abs(v_data[2] - u_data[2]) / 1000,
                       'r': edges[i][2]
                   })
    return G

graphs = []
for i in range(1):
    train = combine_mc_bg(pmc, pbg, events=[idx_len_greater_100[i]], bg_frac=1)
    train = train.reset_index()
    train['index'] = train.index.values
    edges = create_graph(train[['index', 'sx', 'sy', 'sz', 'TX', 'TY']].values, 0.15, 1e3)
    train_np = train[['index', 'sx', 'sy', 'sz', 'TX', 'TY', 'chi2', 'signal']].values
    g = gen_graph_nx(train_np, edges)
    with open('./data/brick_{}.pcl'.format(i), 'wb') as f:
        pickle.dump(file=f, obj=g)
