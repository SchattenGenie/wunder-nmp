%%cython --cplus --compile-args=-fopenmp --compile-args=-O3 --link-args=-fopenmp -f --compile-args=-DCYTHON_TRACE=1 --force
cimport cython
import numpy as np
cimport numpy as np
from cython.parallel import prange
from libc.math cimport sqrt, atan2
from libcpp.vector cimport vector
from libcpp.unordered_map cimport unordered_map
from libcpp.unordered_set cimport unordered_set
@cython.linetrace(True)
@cython.binding(True)
@cython.wraparound(False)
@cython.boundscheck(False)
@cython.cdivision(True)
cdef double sum_sparse_cyt(double[:, :] X, 
                           int[:, :] rows_cols, 
                           unordered_set[int] rows_iterate,
                           unordered_set[int] cols_iterate,
                           unordered_map[int, vector[int]] row_col) nogil:
    cdef double S = 0.
    cdef int N = rows_cols.shape[0]
    cdef int i, R, k
    for i in range(N):
        if (rows_iterate.count(rows_cols[i][0]) + cols_iterate.count(rows_cols[i][1])) < 2:
            continue
        if row_col.count(rows_cols[i][1]) == 0:
            continue
        R = row_col.at(rows_cols[i][1]).size()
        for k in range(R):
            if rows_iterate.count(row_col.at(rows_cols[i][1])[k]):
                S += X[rows_cols[i][0], rows_cols[i][1]] * X[rows_cols[i][1], row_col.at(rows_cols[i][1])[k]]
    return S

@cython.linetrace(True)
@cython.binding(True)
@cython.wraparound(False)
@cython.boundscheck(False)
@cython.cdivision(True)
def sum_sparse_cyt_list(double[:, :] X, 
                        int[:, :] rows_cols, 
                        vector[unordered_set[int]] rows_iterate,
                        vector[unordered_set[int]] cols_iterate,
                        unordered_map[int, vector[int]] row_col):
    cdef vector[double] Ss
    cdef double S
    cdef vector[int] idx
    cdef int N = len(rows_iterate)
    cdef int i
    for i in prange(N, nogil=True, schedule='static', num_threads=22):
        S = sum_sparse_cyt(X, 
                           rows_cols, 
                           rows_iterate[i], 
                           cols_iterate[i],
                           row_col)
        with gil:
            Ss.push_back(S)
            idx.push_back(i)
    return Ss, idx



import itertools
from copy import deepcopy
from itertools import starmap, combinations, product
from collections import defaultdict
from heapq import heappush, heappop

__d_cache = defaultdict(lambda: defaultdict(float))
from collections import Counter
def list_to_idx(ll, d):
    idx = []
    for l in ll:
        idx.append(d_inv[l])
    return idx

def cluster_distance_gdla(cl_1, cl_2, adjacency_matrix, cl_2_cl_1 = None, cl_in=None, cl_out=None):
    """
    If cl_in and cl_out are not None cl_1 supposed to be its union;
    """
    hs_1, hs_2 = hash(cl_1), hash(cl_2)
    C = __d_cache[hs_1][hs_2] + __d_cache[hs_2][hs_1]
    
    if __d_cache[hs_1][hs_2] > 0 and __d_cache[hs_2][hs_1]:
        return -(__d_cache[hs_1][hs_2] > 0 and __d_cache[hs_2][hs_1]), cl_1, cl_2

    if cl_2_cl_1 is not None:
        hs_in, hs_out = hash(frozenset(cl_in)), hash(frozenset(cl_out))
        __d_cache[hs_1][hs_2] = __d_cache[hs_in][hs_2] + __d_cache[hs_out][hs_2]
        __d_cache[hs_2][hs_1] = cl_2_cl_1 / len(cl_1)**2
        #print(np.einsum('ij,jk->', W_12, W_21) / len(cl_1_nodes)**2 - cl_2_cl_1 / len(cl_1)**2)
    else:
        cl_1_nodes = cl_1.listnodes
        cl_2_nodes = cl_2.listnodes
        W_12 = adjacency_matrix[cl_1_nodes, :][:, cl_2_nodes]
        W_21 = adjacency_matrix[cl_2_nodes, :][:, cl_1_nodes]
        __d_cache[hs_1][hs_2] = W_21.dot(W_12).sum() / len(cl_2_nodes)**2
        __d_cache[hs_2][hs_1] = W_12.dot(W_21).sum() / len(cl_1_nodes)**2
        
    return -(__d_cache[hs_1][hs_2] + __d_cache[hs_2][hs_1]), cl_1, cl_2

def nodes_inv_cluster(root, d_inv):
    root.nodes = set([d_inv[node] for node in root.nodes])
    for child in root:
        nodes_inv_cluster(child, d_inv)
        
def run_gdla(G, cl_size):
    __d_cache.clear()
    adjacency_matrix = nx.adjacency_matrix(G)
    sigma = (adjacency_matrix**2).sum() / (adjacency_matrix.shape[0])
    np.exp((-adjacency_matrix.power(2) / sigma).data.data, out=adjacency_matrix.data)
    adjacency_matrix_np = adjacency_matrix.toarray()
    
    
    rows_cols = np.array(list(zip(*adjacency_matrix.nonzero())))
    
    tuples = list(zip(*adjacency_matrix.nonzero()))
    row_col = defaultdict(list)
    for i, (row, col) in enumerate(tuples):
        row_col[row].append(col)
    row_col = dict(row_col)
    row_col = {key: np.array(row_col[key], dtype=np.int32) for key in row_col}

    N = len(adjacency_matrix_np)
    d = {}
    d_inv = {}
    clusters = set()
    for i, node_id in enumerate(G.nodes()):
        d[node_id] = i
        d_inv[i] = node_id
        clusters.add(ClusterHDBSCAN(cl_size=cl_size, weight=np.inf, nodes=[i]))

    heap = []
    for cl_1, cl_2 in combinations(clusters, 2):
        result = cluster_distance_gdla(cl_1, cl_2, adjacency_matrix_np)
        heappush(heap, result)
        
    pbar = tqdm(total=N - 1)
    i = 0
    
    while i < N - 1:
        # lazy delete
        while True:
            weight, cluster_in, cluster_out = heappop(heap)
            if cluster_in in clusters and cluster_out in clusters:
                break
        weight = -weight

        if cluster_in is cluster_out:
            continue
        pbar.update(1)
        clusters.discard(cluster_in)
        clusters.discard(cluster_out)
        cluster_in_old = cluster_in.nodes
        cluster_out_old = cluster_out.nodes
        if cluster_in.is_cluster and cluster_out.is_cluster:
            cluster = ClusterHDBSCAN(weight=weight , cl_size=cl_size, clusters=[cluster_in, cluster_out])
        elif cluster_in.is_cluster and not cluster_out.is_cluster:
            cluster_in_old = cluster_in.nodes.copy()
            cluster = cluster_in.append(weight=weight, clusters=[cluster_out])
        elif cluster_out.is_cluster and not cluster_in.is_cluster:
            cluster_out_old = cluster_out.nodes.copy()
            cluster = cluster_out.append(weight=weight, clusters=[cluster_in])
        else:
            cluster = ClusterHDBSCAN(weight=weight, cl_size=cl_size, clusters=[cluster_in, cluster_out])
        
        average_size = 0
        rows_iterate = len(clusters) * [cluster.npnodes]
        cols_iterate = []
        for cluster_c in clusters:
            cols_iterate.append(cluster_c.npnodes)
            
        #return cluster, clusters, adjacency_matrix_np, rows_cols, np.array(rows_iterate), np.array(cols_iterate), row_col
        dists, idxs = sum_sparse_cyt_list(adjacency_matrix_np, 
                                          rows_cols, 
                                          np.array(rows_iterate), 
                                          np.array(cols_iterate), 
                                          row_col)
        dists = dict(zip(idxs, dists))
        for j, cluster_c in enumerate(clusters):
            result = cluster_distance_gdla(cluster, 
                                           cluster_c, 
                                           adjacency_matrix_np, 
                                           cl_2_cl_1=dists[j],
                                           cl_in=cluster_in_old,
                                           cl_out=cluster_out_old)
            heappush(heap, result)
        clusters.add(cluster)
        i += 1
    pbar.close()
    clusters = list(clusters)

    ### choose biggest cluster
    root = clusters[0]
    
    nodes_inv_cluster(root, d_inv)
    calc_stabilities(root)
    
    #recalc_tree(root)
    #clusters = list(leaf_clusters(root))
    
    return clusters, root